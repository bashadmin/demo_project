#!/bin/bash
read number
counter=1
while [ $counter -le 10 ]
do
	multiple=$(($number*$counter))
	echo "$number * $counter = $multiple"
	counter=$(($counter+1))
done

