#!/bin/bash
# Author : Zahida Meeran
# Write a bash script to:
# Create twenty-five empty (0 KB) files 
# The file names should be <yourName><number>, <yourName><number+1>, <yourName><number+2> and so on.
# Design the script so that each time you execute it, it creates the next batch of 25 files with increasing numbers starting with # # the last or max number that already exists.

fnum=`cat fnum.txt`
lnum=$(($fnum+24))

for i in `eval echo {$fnum..$lnum}`
do
        touch "zee$i"
done
((lnum++))
echo $lnum > fnum.txt
