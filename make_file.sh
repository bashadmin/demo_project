#!/bin/bash
myusername=`whoami`
echo $myusername

#Use double-quotes with sed
lastFilenum=$(ls -l $myusername*  | awk '{ print $9 }' | sed "s/${myusername}//g" | sort -n | tail -1)
echo $lastFilenum

nextFileNum=$(($lastFilenum+1))
echo $nextFileNum

endFileNum=`expr $nextFileNum + 24`

while [ $nextFileNum -le $endFileNum  ]
do
        touch $myusername$nextFileNum
        nextFileNum=$(($nextFileNum+1))
done

