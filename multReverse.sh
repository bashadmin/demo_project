#!/bin/bash
read number
counter=10
while [ $counter -ge 1 ]
do
	multiple=$(($number*$counter))
	echo "$number * $counter = $multiple"
	counter=$(($counter-1))
done

